#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
//#include <time.h>

const char* PAM_DOMAIN = "autox";

static struct pam_conv conv = {
  misc_conv,
  NULL
};

static int verbose_flag;
static char *const default_program[] = { "xinit" };

static struct option long_options[] = {
  {"help",    no_argument, NULL, 'h'},
  {"verbose", no_argument, &verbose_flag, 1},
  {"user",    required_argument, NULL, 'u'},
  {0, 0, 0, 0}
};


void pam_check_ret(pam_handle_t *handle, int ret, char msg[]);
void setup_pam(pam_handle_t *pamh, const struct passwd *pw, const char *tty_name);
int exec_argv(int argc, char *const argv[]);

int main(int argc, char *argv[]) {
  int retval = 0;

/*struct tm *curtime;
  time_t *time;
  curtime = localtime(time);
  fprintf(stderr, "Start time = %i:%i on %i/%i/%i\n", curtime->tm_hour, curtime->tm_min, curtime->tm_mon, curtime->tm_mday, curtime->tm_year); */

  char *user = NULL;
  int option_index = 0;
  int c;
  while ((c = getopt_long (argc, argv, "hu:", long_options, &option_index)) != -1) {
    switch (c) {
      case '0':
        break;
      case 'h':
        printf("Usage: %s --user username [--] [program...]\n", argv[0]);
	exit(0);
        break;
      case 'u':
        user = optarg;
        break;
      case '?':
        // ignore invalid options
        break;
    }
  }

  struct passwd *pw;
  if (user == NULL) {
    fprintf(stderr, "You mush supply a username!\n\te.g `autox -u myuser`\n");
    return (retval = 1);
  }
  else if ((pw = getpwnam(user)) == NULL) {
    fprintf(stderr, "User %s does not exist\n", user);
    return (retval = 1);
  }

  char **program_argv;
  int program_argc;
  if (optind < argc) {
    program_argv = &argv[optind];
    program_argc = argc - optind;
  }
  else {
    program_argv = (char**) default_program;
    program_argc = 1;
  }

  char *tty_name = ttyname(0);
  if (tty_name == NULL) {
    perror("Could not get name of controlling terminal");
    exit(errno);
  }

  /* Check with PAM. */
  pam_handle_t *pamh = NULL;
  /* This must be done before forking, otherwise pam_close_session will return an error */
  setup_pam(pamh, pw, tty_name);

  pid_t cpid = 0;
  pid_t wpid = 0;
  cpid = fork();

  if (cpid == -1) {
    perror("Could not fork child process");
    exit(errno);
  }

  /* Start the user session. */
  if (cpid == 0) {
    /* Setup the user's environment. */
    setenv("HOME", pw->pw_dir, 1);
    setenv("SHELL", pw->pw_shell, 1);
    setenv("USER", pw->pw_name, 1);
    setenv("LOGNAME", pw->pw_name, 1);
    /*setenv("DISPLAY", displayname, 1); */
    chdir(pw->pw_dir);

    /* Change the process' uid and gid. */
    setgid(pw->pw_gid);
    setuid(pw->pw_uid);
    
    /* All systems are a go! */
    exec_argv(program_argc, program_argv);
    perror("Failed to exec() xinit");
    exit(errno);
  }

  /* Wait for the X session to terminate so we can close the PAM session. */
  do {
    wpid = waitpid(cpid, &retval, WUNTRACED | WCONTINUED);
  } while (!WIFEXITED(retval) && !WIFSIGNALED(retval));

  retval = pam_close_session(pamh, 0);
  pam_check_ret(pamh, retval, "Close session error");
  pam_end(pamh, retval);


  return retval;
}

int exec_argv(int argc, char *const argv[]) {
  char *full_argv[argc+1];
  memcpy(full_argv, argv, argc * sizeof(void*));
  full_argv[argc] = NULL;

  if (verbose_flag) {
    fprintf(stderr, "starting program %s\n", argv[0]);
  }
  return execvp(argv[0], full_argv);
}

void setup_pam(pam_handle_t *pamh, const struct passwd *pw, const char *tty_name) {
  int retval = 0;

  /* Start PAM authentication. */
  retval = pam_start(PAM_DOMAIN, pw->pw_name, &conv, &pamh);
  pam_check_ret(pamh, retval, "Open authentication error");

  /* Set the PAM_TTY variable, needed for logind seat management */
  retval = pam_set_item(pamh, PAM_TTY, tty_name);
  pam_check_ret(pamh, retval, "PAM set_item error");

  /* Start PAM authentication. */
  retval = pam_authenticate(pamh, PAM_SILENT);
  pam_check_ret(pamh, retval, "PAM authenticate error");

  /* Make sure the user still has permissions to login. */
  retval = pam_acct_mgmt(pamh, 0);
  pam_check_ret(pamh, retval, "Account management error");

  /* Start the session. */
  retval = pam_open_session(pamh, 0);
  pam_check_ret(pamh, retval, "Open session error");

  /* according to man pam_setcred, setuid/setgid/initgroups should be
   * called BEFORE calling pam_setcred */
  if (setgid(pw->pw_gid) == -1) {
    perror("Could not set the user's primary group");
    exit(errno);
  }
  if (initgroups(pw->pw_name, pw->pw_gid) == -1) {
    perror("Could not set the user's groups");
    exit(errno);
  }

  /* Establish the user's credentials. */
  retval = pam_setcred(pamh, PAM_ESTABLISH_CRED);
  pam_check_ret(pamh, retval, "Establish credentials error");
}

void pam_check_ret(pam_handle_t *handle, int ret, char msg[]) {
  switch (ret) {
    case PAM_SUCCESS:
    case PAM_IGNORE:
      break;
    default:
      fprintf(stderr, "%s: %s\n", msg, pam_strerror(handle, ret));
      pam_end(handle, ret);
      exit(1);
  }
}
